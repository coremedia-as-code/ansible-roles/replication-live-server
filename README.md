# CoreMedia - replication-live-server

## Ports

```
45144
42009
42080
42083
42098
42099
42005
```

## dependent services

### database
```

replication_live_database:
  host: backend_database.int
  port: 3306
  schema: cm_replication
  user: cm_replication
  password: cm_replication
```

### master-live-server
```
replication_live:
  replicator:
    publication:
      ior_url: http://master-live-server.int:40280/master-live-server/ior
```
